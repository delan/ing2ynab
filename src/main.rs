use std::io::{Write, BufRead, BufReader, Lines, stdout, stdin};

use jane_eyre::Result;
use thiserror::Error;
use lazy_static::lazy_static;
use regex::{Regex, Captures};

#[derive(Error, Debug)]
#[error("capture group doesn’t exist or didn’t participate")]
struct CapturesError;

#[derive(Error, Debug)]
#[error("FIXME line error")]
struct LineError;

#[derive(Error, Debug)]
#[error("FIXME month error")]
struct MonthError;

#[derive(Error, Debug)]
#[error("FIXME tag error")]
struct TagError;

trait CapturesExt {
    fn name_ok(&self, name: &str) -> Result<&str>;
}

impl<'t> CapturesExt for Captures<'t> {
    fn name_ok(&self, name: &str) -> Result<&str> {
        Ok(self.name(name).ok_or(CapturesError)?.into())
    }
}

fn main() -> Result<()> {
    process(stdout(), BufReader::new(stdin()))
}

fn process<W: Write, R: BufRead>(mut sink: W, source: R) -> Result<()> {
    let mut lines = source.lines();

    while let Some(line) = lines.next() {
        let line = &line?;

        if let Ok(dtposted) = tag_from_str(line, "<DTPOSTED>") {
            let (trnamt, _) = tag_from_lines(&mut lines, "<TRNAMT>")?;
            let (fitid, i) = tag_from_lines(&mut lines, "<FITID>")?;
            let (memo, j) = tag_from_lines(&mut lines, "<MEMO>")?;

            writeln!(sink, "{}", trnamt)?;

            transaction(&mut sink, dtposted, &fitid[i..], &memo[j..])?;
        } else {
            writeln!(sink, "{}", line)?;
        }
    }

    Ok(())
}

fn tag_from_lines<T: BufRead>(lines: &mut Lines<T>, tag: &str) -> Result<(String, usize)> {
    let line = lines.next().ok_or(LineError)??;
    let _ = tag_from_str(&line, tag)?;

    Ok((line, tag.len()))
}

fn tag_from_str<'a>(line: &'a str, tag: &str) -> Result<&'a str> {
    if !line.starts_with(tag) {
        return Err(TagError)?;
    }

    Ok(&line[tag.len()..])
}

fn transaction<W: Write>(sink: &mut W, dtposted: &str, fitid: &str, memo: &str) -> Result<()> {
    lazy_static! {
        static ref CARD: Regex = Regex::new(r"(?x)^
            (?P<payee>.*)
            \ -\ (Visa|EFTPOS)\ (Purchase|Refund)\ -\ Receipt
            \ (?P<receipt>\d+)
            (?P<memo>
                (?:
                    Foreign\ Currency\ Amount:
                    \ (?:\ 0|(?P<foreign>[A-Z]{3}\ \d+(?:[.]\d+)?))
                )?
                \ ?(?:In\ (?P<locality>.*)\ )?Date
                \ (?P<day>\d{2})
                \ (?P<month>Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)
                \ (?P<year>\d+)
                .*
            )
        ").unwrap();

        static ref LOAN: Regex = Regex::new(r"(?x)^
            (?P<memo>(?:
                Repayment\ -\ Direct\ Debit
                |Interest\ Charge
                |Redraw
                |Deposit
                |Advance
            ))
            \ -\ Receipt\ No
            \ (?P<receipt>\d+)
            (?P<payee>.*)
        ").unwrap();

        static ref OTHER: Regex = Regex::new(r"(?x)^
            (?P<payee>.*)
            \ -\ Receipt
            (?:\ No)?
            \ (?P<receipt>\d+)
            (?P<memo>.*)
        ").unwrap();

        static ref MONTH: [&'static str; 12] = [
            "Jan", "Feb", "Mar", "Apr",
            "May", "Jun", "Jul", "Aug",
            "Sep", "Oct", "Nov", "Dec",
        ];
    }

    if let Some(visa) = CARD.captures(memo) {
        assert!(fitid == visa.name_ok("receipt")?);

        let day = usize::from_str_radix(visa.name_ok("day")?, 10)?;
        let month = visa.name_ok("month")?;
        let month = MONTH.iter().position(|&x| x == month).ok_or(MonthError)? + 1;
        let year = usize::from_str_radix(visa.name_ok("year")?, 10)?;

        writeln!(sink, "<NAME>{}", visa.name_ok("payee")?.trim())?;
        writeln!(sink, "<MEMO>[ing2ynab] {}", visa.name_ok("memo")?.trim())?;
        writeln!(sink, "<DTPOSTED>{:04}{:02}{:02}000000", year, month, day)?;
    } else {
        if let Some(other) = LOAN.captures(memo).or_else(|| OTHER.captures(memo)) {
            assert!(fitid == other.name_ok("receipt")?);

            writeln!(sink, "<NAME>{}", other.name_ok("payee")?.trim())?;
            writeln!(sink, "<MEMO>[ing2ynab] {}", other.name_ok("memo")?.trim())?;
        } else {
            writeln!(sink, "<MEMO>[ing2ynab] {}", memo)?;
        }

        writeln!(sink, "<DTPOSTED>{}", dtposted)?;
    }

    writeln!(sink, "<FITID>{}", fitid)?;

    Ok(())
}

#[test] fn test() -> Result<()> {
    use std::io::{Read, Cursor};
    use std::fs::File;

    let source = BufReader::new(File::open("examples/in.ofx")?);
    let mut sink = Cursor::new(vec![0u8; 0]);
    process(&mut sink, source)?;

    let mut expected = vec![0u8; 0];
    File::open("examples/out.ofx")?.read_to_end(&mut expected)?;

    assert_eq!(sink.into_inner(), expected);

    Ok(())
}
