# 0.0.5 (2020-11-21)


- added support for Personal Loan transactions
- increased minimum Rust version from 1.37 to 1.43


# 0.0.4 (2020-06-30)


# 0.0.3 (2019-07-05)


- added support for Visa Refund transactions
- added support for EFTPOS Purchase transactions


# 0.0.2 (2019-07-04)


# 0.0.1 (2019-07-04)


# 0.0.0 (2019-07-04)


- initial release
