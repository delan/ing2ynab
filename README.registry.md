ing2ynab
========


[![crates.io page](https://img.shields.io/crates/v/ing2ynab)](https://crates.io/crates/ing2ynab)
[![Bitbucket Pipelines status](https://img.shields.io/bitbucket/pipelines/delan/ing2ynab/default)](https://bitbucket.org/delan/ing2ynab/addon/pipelines/home#!/results/branch/default/page/1)


- [Release notes](https://bitbucket.org/delan/ing2ynab/src/default/RELEASES.md)


ing2ynab(1) cleans up ing.com.au transactions for YNAB.


    cargo install ing2ynab
    ing2ynab < Transactions.ofx > YNAB.ofx


*Extracts payee information out of the memo column:* this helps YNAB
automatically choose a payee for your transactions, because having
everything in the memo column makes every transaction unique, which
doesn’t play nice with “Is” renaming rules.


*Changes the dates of debit card transactions to the authorised date:*
the cleared date makes it easier to verify your balance up to a given
date, but the authorised date is more consistent with the workflow of
entering transactions as you go, then importing and matching.


![screenshot](https://bitbucket.org/delan/ing2ynab/raw/0.0.5/examples/out.png)


## How to export transactions from Secure Banking


In the Export menu at the top of a single account’s transaction list,
choose “OFX (MYOB, MS Money)”. The combined transaction list on your
home page won’t have that option.


![good](https://bitbucket.org/delan/ing2ynab/raw/0.0.5/good.png)


![bad](https://bitbucket.org/delan/ing2ynab/raw/0.0.5/bad.png)


## Requirements


- Rust 1.43+ (tested with 1.43.0)
